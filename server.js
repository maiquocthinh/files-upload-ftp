const express = require("express");
const fileUpload = require("express-fileupload");
const FTPClient = require("ftp");
const cors = require("cors");
require("dotenv").config();
const app = express();

app.use(
  cors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    credentials: true,
    optionsSuccessStatus: 200,
  })
);

app.use(fileUpload());

const putFile = (client, file) =>
  new Promise((resolve, rejects) => {
    const name = file.md5 + "-" + file.name;
    client.put(file.data, process.env.UPLOAD_PATH + name, (err) => {
      if (err) rejects(err);
      resolve({
        name: file.name,
        size: file.size,
        mimeType: file.mimetype,
        url: process.env.UPLOAD_URL + encodeURI(name),
      });
    });
  });

app.post("/upload", async (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0)
    return res.status(400).json({ msg: "No files were uploaded." });

  const files = req.files.files;
  const client = new FTPClient();

  client.connect({
    host: process.env.FTP_HOST,
    user: process.env.FTP_USER,
    password: process.env.FTP_PASS,
  });

  client.on("ready", async () => {
    if (Array.isArray(files)) {
      const promiseArr = [];
      files.forEach((file) => {
        promiseArr.push(putFile(client, file));
      });
      const result = await Promise.all(promiseArr);
      res.status(200).json({
        msg: "Upload successully.",
        data: result,
      });
    } else {
      const result = await putFile(client, files);
      res.status(200).json({
        msg: "Upload successully.",
        data: result,
      });
    }

    client.end();
  });

  client.on("error", (error) => {
    return res.status(500).json({ msg: error.message });
  });
});

app.listen(process.env.PORT || 8000, () => {
  console.log("App running...");
});
